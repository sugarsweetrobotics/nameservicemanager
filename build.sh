PACKAGE_NAME=SystemEditor
ICON_FILE=icon.png
APP_FILE=application.yaml

rm -rf $PACKAGE_NAME
pub build
mv build $PACKAGE_NAME
cp $ICON_FILE $PACKAGE_NAME/
cp $APP_FILE $PACKAGE_NAME/
zip -rq $PACKAGE_NAME.zip $PACKAGE_NAME
mv $PACKAGE_NAME.zip `wasanbon-admin.py web package_dir`
