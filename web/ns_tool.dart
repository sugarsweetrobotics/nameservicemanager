
@HtmlImport('ns_tool.html')
library ns_tool;

import 'dart:html' as html;
import 'package:web_components/web_components.dart' show HtmlImport;
import 'package:polymer/polymer.dart';

import 'package:wasanbon_elements/nameservice/system_editor.dart';
import 'package:wasanbon_elements/nameservice/host_ns_manager.dart';
import 'nameservicemanager.dart';
import 'package:polymer_elements/paper_dialog.dart';
import 'package:wasanbon_elements/wasanbon_toolbar.dart';

import 'package:wasanbon_elements/message_dialog.dart';


@PolymerRegister('ns-tool')
class NSTool extends PolymerElement {
  HostNSManager hostNSManager;
  SystemEditor systemEditor;

  NSTool.created() : super.created();

  void attached() {
    ($$('#toolbar') as WasanbonToolbar).onBack.listen((var e) {
      onBack();
    });

      hostNSManager = $$('#host-ns');
      systemEditor = $$('#system-editor');

      hostNSManager.rpc = rpc;
      systemEditor.rpc = rpc;
  }

  void onBack() {
    ConfirmDialog dlg = $$('#message-dlg');
    dlg.ptr.onOK.listen((var dlg_) {
      //html.window.history.back();
      html.window.location.href = 'http://${Uri.base.host}:${Uri.base.port}';
    });
    dlg.show('Confirm', 'Really exit from NameService Manager?');
  }

}
